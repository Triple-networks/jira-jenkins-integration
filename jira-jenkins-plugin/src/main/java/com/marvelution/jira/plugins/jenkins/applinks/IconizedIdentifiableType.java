/*
 * Jenkins Plugin for JIRA
 * Copyright (C) 2012 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.jira.plugins.jenkins.applinks;

import com.atlassian.applinks.spi.application.IdentifiableType;
import com.atlassian.plugin.webresource.UrlMode;
import com.atlassian.plugin.webresource.WebResourceUrlProvider;
import com.marvelution.jira.plugins.jenkins.utils.JenkinsPluginUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URI;
import java.net.URISyntaxException;

/**
 * New Abstract implementation of the {@link IdentifiableType} interface that implemented a standard getIconUrl()
 * method
 *
 * @author Mark Rekveld
 * @since 1.1.0
 */
public abstract class IconizedIdentifiableType implements IdentifiableType {

	private static final Logger LOGGER = LoggerFactory.getLogger(IconizedIdentifiableType.class);
	private final JenkinsPluginUtil pluginUtil;
	private final WebResourceUrlProvider webResourceUrlProvider;

	protected IconizedIdentifiableType(JenkinsPluginUtil pluginUtil, WebResourceUrlProvider webResourceUrlProvider) {
		this.pluginUtil = pluginUtil;
		this.webResourceUrlProvider = webResourceUrlProvider;
	}

	/**
	 * Get the Icon {@link URI} for this {@link IdentifiableType}
	 *
	 * @return the icon {@link URI}
	 */
	public final URI getIconUrl() {
		try {
			return new URI(webResourceUrlProvider.getStaticPluginResourceUrl(pluginUtil.getPluginKey()
					+ ":jenkins-images", "images", UrlMode.ABSOLUTE) + "/icon16_" + getId().get() + ".png");
		} catch (URISyntaxException e) {
			LOGGER.warn("Unable to find the icon for this application type.", e);
		}
		return null;
	}

}
