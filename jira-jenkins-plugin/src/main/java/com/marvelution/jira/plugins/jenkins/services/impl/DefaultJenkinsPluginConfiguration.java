/*
 * Jenkins Plugin for JIRA
 * Copyright (C) 2012 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.jira.plugins.jenkins.services.impl;

import com.atlassian.applinks.host.spi.HostApplication;
import com.atlassian.jira.project.Project;
import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.marvelution.jira.plugins.jenkins.services.JenkinsPluginConfiguration;

import java.util.List;
import java.util.Set;

/**
 * Default {@link JenkinsPluginConfiguration} implementation
 *
 * @author Mark Rekveld
 * @since 1.6.0
 */
public class DefaultJenkinsPluginConfiguration implements JenkinsPluginConfiguration {

	static final String SETTING_PREFIX = "jji.";
	static final String MAX_BUILDS_PER_PAGE = SETTING_PREFIX + "max_builds_per_page";
	static final String ENABLED_PROJECT_KEYS = SETTING_PREFIX + "enabled_project_keys";
	private final HostApplication hostApplication;
	private final PluginSettingsFactory pluginSettingsFactory;
	private transient PluginSettings pluginSettings;

	public DefaultJenkinsPluginConfiguration(HostApplication hostApplication, PluginSettingsFactory pluginSettingsFactory) {
		this.hostApplication = hostApplication;
		this.pluginSettingsFactory = pluginSettingsFactory;
	}

	@Override
	public String getJiraBaseUrl() {
		return hostApplication.getBaseUrl().toString();
	}

	@Override
	public int getMaximumBuildsPerPage() {
		Integer count;
		try {
			count = Integer.parseInt((String) getPluginSettings().get(MAX_BUILDS_PER_PAGE));
		} catch (Exception e) {
			count = DEFAULT_MAX_BUILDS_PER_PAGE;
		}
		return count;
	}

	@Override
	public void setMaximumBuildsPerPage(Integer maximumBuildsPerPage) {
		if (maximumBuildsPerPage == null || DEFAULT_MAX_BUILDS_PER_PAGE == maximumBuildsPerPage ||
				(maximumBuildsPerPage < 1 && maximumBuildsPerPage != -1)) {
			getPluginSettings().remove(MAX_BUILDS_PER_PAGE);
		} else {
			getPluginSettings().put(MAX_BUILDS_PER_PAGE, String.valueOf(maximumBuildsPerPage));
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	public Set<String> getEnabledProjects() {
		List<String> keys = (List<String>) getPluginSettings().get(ENABLED_PROJECT_KEYS);
		return keys == null ? null : Sets.newHashSet(keys);
	}

	@Override
	public void setEnabledProjects(Set<String> enabledProjects) {
		if (enabledProjects == null || enabledProjects.isEmpty()) {
			getPluginSettings().remove(ENABLED_PROJECT_KEYS);
		} else {
			getPluginSettings().put(ENABLED_PROJECT_KEYS, Lists.newArrayList(enabledProjects));
		}
	}

	@Override
	public boolean isProjectEnabled(Project project) {
		Set<String> enabledProjects = getEnabledProjects();
		return enabledProjects == null || enabledProjects.isEmpty() || enabledProjects.contains(project.getKey());
	}

	@Override
	public boolean isProjectEnabledExplicitly(Project project) {
		Set<String> enabledProjects = getEnabledProjects();
		return enabledProjects != null && enabledProjects.contains(project.getKey());
	}

	PluginSettings getPluginSettings() {
		if (pluginSettings == null) {
			pluginSettings = pluginSettingsFactory.createGlobalSettings();
		}
		return pluginSettings;
	}

}
