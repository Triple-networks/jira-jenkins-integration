/*
 * Jenkins Plugin for JIRA
 * Copyright (C) 2012 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.jira.plugins.jenkins.panels;

import com.atlassian.jira.issue.search.SearchProvider;
import com.atlassian.jira.plugin.versionpanel.BrowseVersionContext;
import com.atlassian.jira.plugin.versionpanel.impl.GenericTabPanel;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.web.FieldVisibilityManager;
import com.marvelution.jira.plugins.jenkins.model.Build;

import java.util.List;
import java.util.Map;

/**
 * The {@link GenericTabPanel} implementation to get all the {@link Build}s related to the {@link }
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
public class JenkinsVersionPanel extends GenericTabPanel {

	private final BuildPanelHelper buildPanelHelper;

	public JenkinsVersionPanel(JiraAuthenticationContext authenticationContext, SearchProvider searchProvider,
	                           FieldVisibilityManager fieldVisibilityManager, BuildPanelHelper buildPanelHelper) {
		super(authenticationContext, searchProvider, fieldVisibilityManager);
		this.buildPanelHelper = buildPanelHelper;
	}

	@Override
	protected Map<String, Object> createVelocityParams(BrowseVersionContext ctx) {
		Map<String, Object> params = super.createVelocityParams(ctx);
		Iterable<? extends Build> builds = buildPanelHelper.getBuildsByRelation(ctx.getVersion(), ctx.getUser());
		List<BuildAction> buildActions = buildPanelHelper.getBuildActions(builds, true);
		params.put("actions", buildActions);
		return params;
	}

	@Override
	public boolean showPanel(BrowseVersionContext ctx) {
		return buildPanelHelper.showPanel(ctx.getProject(), ctx.getUser());
	}

}
