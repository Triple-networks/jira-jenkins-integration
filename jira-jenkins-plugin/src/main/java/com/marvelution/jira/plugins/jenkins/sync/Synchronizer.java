/*
 * Jenkins Plugin for JIRA
 * Copyright (C) 2012 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.jira.plugins.jenkins.sync;

import com.marvelution.jira.plugins.jenkins.model.Job;
import com.marvelution.jira.plugins.jenkins.model.Progress;

/**
 * Synchronizer interface for synchronizing builds of jobs
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
public interface Synchronizer {

	/**
	 * Synchronize the given {@link Job} using the given {@link SynchronizationOperation}
	 *
	 * @param job       the {@link Job} to sychronize
	 * @param operation the {@link SynchronizationOperation} to execute
	 */
	void synchronize(Job job, SynchronizationOperation operation);

	/**
	 * Request the {@link SynchronizationOperation} of the given {@link Job} to stop
	 *
	 * @param job the {@link Job} to stop the synchronization of
	 */
	void stopSynchronization(Job job);

	/**
	 * Getter for the {@link Progress} of the {@link SynchronizationOperation} of the given {@link Job}
	 *
	 * @param job the {@link Job} to get the {@link Progress} status of
	 * @return the {@link Progress}
	 */
	Progress getProgress(Job job);

}
