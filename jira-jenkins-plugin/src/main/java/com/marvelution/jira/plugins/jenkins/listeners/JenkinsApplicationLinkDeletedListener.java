/*
 * Jenkins Plugin for JIRA
 * Copyright (C) 2012 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.jira.plugins.jenkins.listeners;

import com.atlassian.applinks.api.event.ApplicationLinkDeletedEvent;
import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
import com.marvelution.jira.plugins.jenkins.services.SiteService;

/**
 * {@link EventListener} implementation to update the data on an {@link ApplicationLinkDeletedEvent}
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
public class JenkinsApplicationLinkDeletedListener extends AbstractJenkinsApplicationLinkListener {

	private final SiteService siteService;

	public JenkinsApplicationLinkDeletedListener(EventPublisher eventPublisher, SiteService siteService) {
		super(eventPublisher);
		this.siteService = siteService;
	}

	/**
	 * {@link EventListener} for {@link ApplicationLinkDeletedEvent}
	 * events
	 *
	 * @param event the {@link ApplicationLinkDeletedEvent} event
	 */
	@EventListener
	public void onApplicationLinkDeleted(ApplicationLinkDeletedEvent event) {
		siteService.getAll(false);
	}

}
