/*
 * Jenkins Plugin for JIRA
 * Copyright (C) 2012 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.jira.plugins.jenkins.scheduler;

import com.atlassian.scheduler.compat.JobHandler;
import com.atlassian.scheduler.compat.JobHandlerKey;
import com.atlassian.scheduler.compat.JobInfo;
import com.marvelution.jira.plugins.jenkins.model.Site;
import com.marvelution.jira.plugins.jenkins.services.Communicator;
import com.marvelution.jira.plugins.jenkins.services.CommunicatorFactory;
import com.marvelution.jira.plugins.jenkins.services.JobService;
import com.marvelution.jira.plugins.jenkins.services.SiteService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Jenkins Scheduled Synchronisation Job
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
public class JenkinsSynchronisationJob implements JobHandler {

	public static final JobHandlerKey JOB_KEY = JobHandlerKey.of(JenkinsSynchronisationJob.class.getName());
	private static final Logger LOGGER = LoggerFactory.getLogger(JenkinsSynchronisationJob.class);
	private final SiteService siteService;
	private final JobService jobService;
	private final CommunicatorFactory communicatorFactory;

	public JenkinsSynchronisationJob(SiteService siteService, JobService jobService, CommunicatorFactory communicatorFactory) {
		this.siteService = siteService;
		this.jobService = jobService;
		this.communicatorFactory = communicatorFactory;
	}

	@Override
	public void execute(JobInfo jobInfo) {
		try {
			for (Site site : siteService.getAll(false)) {
				Communicator communicator = communicatorFactory.get(site);
				site.setSupportsBackLink(communicator.isJenkinsPluginInstalled());
				siteService.save(site);
				jobService.syncJobList(site);
			}
		} catch (Throwable t) {
			LOGGER.warn("Failed to execute the Jenkins Scheduled Job; {}", t.getMessage());
		}
	}

}
