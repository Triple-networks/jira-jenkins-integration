/*
 * Jenkins Plugin for JIRA
 * Copyright (C) 2012 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.jira.plugins.jenkins.scheduler;

import com.atlassian.sal.api.lifecycle.LifecycleAware;
import com.atlassian.scheduler.compat.CompatibilityPluginScheduler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;

import java.util.Date;
import java.util.concurrent.TimeUnit;

import static com.marvelution.jira.plugins.jenkins.scheduler.JenkinsSynchronisationJob.JOB_KEY;

/**
 * Jenkins Scheduler implementation
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
public class JenkinsScheduler implements LifecycleAware, DisposableBean {

	private static final Logger LOGGER = LoggerFactory.getLogger(JenkinsScheduler.class);
	private static final String PROPERTY_KEY = "jenkins.connector.scheduler.interval";
	private static final long DEFAULT_INTERVAL = TimeUnit.HOURS.toMillis(1);
	private final CompatibilityPluginScheduler compatibilityPluginScheduler;
	private final JenkinsSynchronisationJob synchronisationJob;

	public JenkinsScheduler(CompatibilityPluginScheduler compatibilityPluginScheduler, JenkinsSynchronisationJob synchronisationJob) {
		this.compatibilityPluginScheduler = compatibilityPluginScheduler;
		this.synchronisationJob = synchronisationJob;
	}

	@Override
	public void onStart() {
		LOGGER.info("Starting the Jenkins Scheduler");
		String property = System.getProperty(PROPERTY_KEY, Long.toString(DEFAULT_INTERVAL));
		long interval;
		try {
			interval = Long.parseLong(property);
		} catch (Exception e) {
			interval = DEFAULT_INTERVAL;
		}
		LOGGER.debug("Starting Jenkins synchronisation job, interval = " + interval);
		try {
			compatibilityPluginScheduler.registerJobHandler(JOB_KEY, synchronisationJob);
		} catch (Exception e) {
			LOGGER.debug("Job Handler with key {} is already registered.", JOB_KEY);
		}
		try {
			compatibilityPluginScheduler.scheduleClusteredJob(JOB_KEY.toString(), JOB_KEY, new Date(), interval);
		} catch (Exception e) {
			LOGGER.error("Failed to schedule Jenkins synchronisation job!", e.getMessage());
		}
	}

	/**
	 * Cleanup just before the bean is set to be destroyed.
	 * This will unschedule the synchronization job scheduled on start
	 */
	@Override
	public void destroy() throws Exception {
		LOGGER.info("Stopping the Jenkins Scheduler");
		compatibilityPluginScheduler.unscheduleClusteredJob(JOB_KEY.toString());
	}

}
