/*
 * Jenkins Plugin for JIRA
 * Copyright (C) 2012 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.jira.plugins.jenkins.applinks;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.auth.AuthenticationProvider;
import com.atlassian.applinks.api.auth.types.BasicAuthenticationProvider;
import com.atlassian.applinks.spi.Manifest;
import com.atlassian.applinks.spi.application.ApplicationIdUtil;
import com.atlassian.applinks.spi.application.NonAppLinksApplicationType;
import com.atlassian.applinks.spi.application.TypeId;
import com.google.common.collect.ImmutableSet;
import org.osgi.framework.Version;

import java.net.URI;
import java.util.Collections;
import java.util.Set;

/**
 * Default {@link com.atlassian.applinks.spi.Manifest} used by the plugin
 *
 * @author Mark Rekveld
 * @since 1.4.3
 */
public class DefaultManifest implements Manifest {

	private final NonAppLinksApplicationType applicationType;
	private final URI url;

	public DefaultManifest(NonAppLinksApplicationType applicationType, URI url) {
		this.applicationType = applicationType;
		this.url = url;
	}

	@Override
	public ApplicationId getId() {
		return ApplicationIdUtil.generate(url);
	}

	@Override
	public String getName() {
		return applicationType.getI18nKey();
	}

	@Override
	public TypeId getTypeId() {
		return applicationType.getId();
	}

	@Override
	public String getVersion() {
		return null;
	}

	@Override
	public Long getBuildNumber() {
		return null;
	}

	@Override
	public URI getUrl() {
		return url;
	}

	public URI getIconUrl() {
		return applicationType.getIconUrl();
	}

	@Override
	public Version getAppLinksVersion() {
		return null;
	}

	@Override
	public Set<Class<? extends AuthenticationProvider>> getInboundAuthenticationTypes() {
		return ImmutableSet.<Class<? extends AuthenticationProvider>>of(BasicAuthenticationProvider.class);
	}

	@Override
	public Set<Class<? extends AuthenticationProvider>> getOutboundAuthenticationTypes() {
		return Collections.emptySet();
	}

	@Override
	public Boolean hasPublicSignup() {
		return null;
	}

}
