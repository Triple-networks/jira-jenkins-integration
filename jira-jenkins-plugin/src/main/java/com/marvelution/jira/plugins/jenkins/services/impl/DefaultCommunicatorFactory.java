/*
 * Jenkins Plugin for JIRA
 * Copyright (C) 2012 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.jira.plugins.jenkins.services.impl;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationLinkService;
import com.atlassian.applinks.api.TypeNotInstalledException;
import com.atlassian.applinks.host.spi.HostApplication;
import com.atlassian.applinks.spi.auth.AuthenticationConfigurationManager;
import com.marvelution.jira.plugins.jenkins.applinks.HudsonApplicationType;
import com.marvelution.jira.plugins.jenkins.applinks.JenkinsApplicationType;
import com.marvelution.jira.plugins.jenkins.dao.SiteDao;
import com.marvelution.jira.plugins.jenkins.model.Site;
import com.marvelution.jira.plugins.jenkins.services.Communicator;
import com.marvelution.jira.plugins.jenkins.services.CommunicatorFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Default implementation of {@link CommunicatorFactory}
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
public class DefaultCommunicatorFactory implements CommunicatorFactory {

	private static final Logger LOGGER = LoggerFactory.getLogger(DefaultCommunicatorFactory.class);
	private final HostApplication hostApplication;
	private final ApplicationLinkService applicationLinkService;
	private final AuthenticationConfigurationManager authenticationConfigurationManager;
	private final SiteDao siteDao;

	public DefaultCommunicatorFactory(HostApplication hostApplication, ApplicationLinkService applicationLinkService,
	                                  AuthenticationConfigurationManager authenticationConfigurationManager, SiteDao siteDao) {
		this.hostApplication = hostApplication;
		this.applicationLinkService = applicationLinkService;
		this.authenticationConfigurationManager = authenticationConfigurationManager;
		this.siteDao = siteDao;
	}

	@Override
	public Communicator get(int siteId) {
		return get(siteDao.get(siteId));
	}

	@Override
	public Communicator get(Site site) {
		checkNotNull(site, "site cannot be null");
		return getByApplicationId(site, site.getApplicationId());
	}

	@Override
	public Communicator get(ApplicationId applicationId) {
		checkNotNull(applicationId, "applicationId cannot be null");
		return getByApplicationId(siteDao.get(applicationId), applicationId);
	}

	@Override
	public Communicator get(ApplicationLink applicationLink) {
		checkNotNull(applicationLink, "applicationLink cannot be null");
		return getByApplicationLink(siteDao.get(applicationLink.getId()), applicationLink);
	}

	/**
	 * Get a {@link Communicator} instance for the given {@link Site} and {@link ApplicationId} given
	 *
	 * @param site          the {@link Site}
	 * @param applicationId the {@link ApplicationId}
	 * @return teh {@link Communicator} instance
	 */
	private Communicator getByApplicationId(Site site, ApplicationId applicationId) {
		checkNotNull(applicationId, "applicationId cannot be null");
		try {
			return getByApplicationLink(site, applicationLinkService.getApplicationLink(applicationId));
		} catch (TypeNotInstalledException e) {
			throw new IllegalArgumentException("Unable to get an ApplicationLink for " + applicationId);
		}
	}

	/**
	 * Get a {@link Communicator} instance for the given {@link Site} and {@link ApplicationLink} given
	 *
	 * @param site            the {@link Site}
	 * @param applicationLink the {@link ApplicationLink}
	 * @return teh {@link Communicator} instance
	 */
	private Communicator getByApplicationLink(Site site, ApplicationLink applicationLink) {
		checkNotNull(site, "site cannot be null");
		checkNotNull(applicationLink, "applicationLink cannot be null");
		LOGGER.debug("Getting a Communicator for ApplicationLink '" + applicationLink.getName() + "' of type " + applicationLink.getType
				().getI18nKey());
		if (applicationLink.getType() instanceof JenkinsApplicationType || applicationLink.getType() instanceof HudsonApplicationType) {
			return new RestCommunicator(hostApplication, site, applicationLink, authenticationConfigurationManager);
		} else {
			throw new IllegalArgumentException(applicationLink.getName() + " is not a Jenkins Application Link");
		}
	}

}
