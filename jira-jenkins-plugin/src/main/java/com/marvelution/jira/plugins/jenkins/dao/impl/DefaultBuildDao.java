/*
 * Jenkins Plugin for JIRA
 * Copyright (C) 2012 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.jira.plugins.jenkins.dao.impl;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.jira.plugins.dvcs.dao.impl.MapRemovingNullCharacterFromStringValues;
import com.atlassian.sal.api.transaction.TransactionCallback;
import com.google.common.base.Function;
import com.google.common.collect.Lists;
import com.marvelution.jira.plugins.jenkins.ao.BuildMapping;
import com.marvelution.jira.plugins.jenkins.ao.IssueMapping;
import com.marvelution.jira.plugins.jenkins.dao.BuildDao;
import com.marvelution.jira.plugins.jenkins.model.Build;
import com.marvelution.jira.plugins.jenkins.services.BuildIssueFilter;
import com.marvelution.jira.plugins.jenkins.services.impl.BuildIssueFilterQueryBuilder;
import net.java.ao.Query;
import org.apache.commons.lang.math.Range;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.google.common.collect.Collections2.transform;
import static com.google.common.collect.Sets.newHashSet;
import static com.marvelution.jira.plugins.jenkins.ao.BuildMapping.*;

/**
 * Default {@link BuildDao} implementation
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
public class DefaultBuildDao implements BuildDao {

	private static final Logger LOGGER = LoggerFactory.getLogger(DefaultBuildDao.class);
	private static final int MAX_SIZE = 200;
	private final Function<BuildMapping, Build> buildMappingToBuildFunction = new Function<BuildMapping, Build>() {
		@Override
		public Build apply(@Nullable BuildMapping from) {
			if (from == null) {
				return null;
			}
			Build build = new Build(from.getID(), from.getJobId(), from.getBuildNumber());
			build.setDisplayName(from.getDisplayName());
			build.setCause(from.getCause());
			build.setDuration(from.getDuration());
			build.setTimestamp(from.getTimeStamp());
			build.setResult(from.getResult());
			build.setBuiltOn(from.getBuiltOn());
			build.setDeleted(from.isDeleted());
			return build;
		}
	};
	private final ActiveObjects ao;
	private final BuildIssueFilterQueryBuilder queryBuilder;

	public DefaultBuildDao(ActiveObjects ao, BuildIssueFilterQueryBuilder queryBuilder) {
		this.ao = ao;
		this.queryBuilder = queryBuilder;
	}

	@Override
	public Build get(final int buildId) {
		BuildMapping mapping = ao.executeInTransaction(new TransactionCallback<BuildMapping>() {
			@Override
			public BuildMapping doInTransaction() {
				return ao.get(BuildMapping.class, buildId);
			}
		});
		return buildMappingToBuildFunction.apply(mapping);
	}

	@Override
	public Build get(final int jobId, final int buildNumber) {
		BuildMapping mapping = ao.executeInTransaction(new TransactionCallback<BuildMapping>() {
			@Override
			public BuildMapping doInTransaction() {
				BuildMapping[] mappings = ao.find(BuildMapping.class, Query.select().where(JOB_ID + " =  ? AND " +
						BUILD_NUMBER + " = ?", jobId, buildNumber));
				if (mappings != null && mappings.length == 1) {
					return mappings[0];
				} else {
					return null;
				}
			}
		});
		return buildMappingToBuildFunction.apply(mapping);
	}

	@Override
	public Iterable<Build> getAllInRange(final int jobId, final Range buildRange) {
		List<BuildMapping> mappings = ao.executeInTransaction(new TransactionCallback<List<BuildMapping>>() {
			@Override
			public List<BuildMapping> doInTransaction() {
				return Arrays.asList(ao.find(BuildMapping.class, Query.select()
						.where(JOB_ID + " = ? AND " + BUILD_NUMBER + " >= ? AND " + BUILD_NUMBER +
								" <= ?", jobId, buildRange.getMinimumInteger(), buildRange.getMaximumInteger())
						.order(TIME_STAMP)));
			}
		});
		return Lists.transform(mappings, buildMappingToBuildFunction);
	}

	@Override
	public Iterable<Build> getAllByJob(final int jobId) {
		List<BuildMapping> mappings = ao.executeInTransaction(new TransactionCallback<List<BuildMapping>>() {
			@Override
			public List<BuildMapping> doInTransaction() {
				return Arrays.asList(ao.find(BuildMapping.class, Query.select()
						.where(JOB_ID + " = ?", jobId)
						.order(TIME_STAMP)));
			}
		});
		return Lists.transform(mappings, buildMappingToBuildFunction);
	}

	@Override
	public Iterable<Build> getByIssueKey(String issueKey) {
		BuildIssueFilter filter = new BuildIssueFilter();
		filter.setInIssueKeys(newHashSet(issueKey));
		return getLatestByFilter(-1, filter);
	}

	@Override
	public Iterable<Build> getByProjectKey(String projectKey) {
		BuildIssueFilter filter = new BuildIssueFilter();
		filter.setInProjectKeys(newHashSet(projectKey));
		return getLatestByFilter(-1, filter);
	}

	@Override
	public Iterable<Build> getLatestByFilter(final int maxResults, BuildIssueFilter filter) {
		final Query query = queryBuilder.toBuildMappingQuery(filter, getIssueMappingsByFilter(maxResults, filter));
		if (query != null) {
			return newHashSet(transform(ao.executeInTransaction(new TransactionCallback<Set<BuildMapping>>() {
				@Override
				public Set<BuildMapping> doInTransaction() {
					return newHashSet(ao.find(BuildMapping.class, query.limit(maxResults)));
				}
			}), buildMappingToBuildFunction));
		} else {
			return newHashSet();
		}
	}

	/**
	 * Returns all the {@link IssueMapping}s that match the given {@code filter}
	 *
	 * @param maxResults the maximum number of results to return
	 * @param filter     the {@link BuildIssueFilter}
	 * @return the {@link List} of {@link IssueMapping}s that match the given {@code filter}
	 */
	private List<IssueMapping> getIssueMappingsByFilter(final int maxResults, BuildIssueFilter filter) {
		final Query query = queryBuilder.toIssueMappingQuery(filter);
		if (query != null) {
			return ao.executeInTransaction(new TransactionCallback<List<IssueMapping>>() {
				@Override
				public List<IssueMapping> doInTransaction() {
					return Arrays.asList(ao.find(IssueMapping.class, query.limit(maxResults)));
				}
			});
		} else {
			return Lists.newArrayList();
		}
	}

	@Override
	public Build save(final Build build) {
		LOGGER.debug("Saving {}", build.toString());
		BuildMapping mapping = ao.executeInTransaction(new TransactionCallback<BuildMapping>() {
			@Override
			public BuildMapping doInTransaction() {
				BuildMapping mapping;
				if (build.getId() == 0) {
					Map<String, Object> map = new MapRemovingNullCharacterFromStringValues();
					map.put(BuildMapping.JOB_ID, build.getJobId());
					map.put(BuildMapping.BUILD_NUMBER, build.getNumber());
					if (StringUtils.length(build.getDisplayName()) <= MAX_SIZE) {
						map.put(BuildMapping.DISPLAY_NAME, build.getDisplayName());
					} else {
						LOGGER.warn("Display name of build {} of job {} is to big to store...", build.getNumber(), build.getJobId());
					}
					map.put(BuildMapping.CAUSE, StringUtils.substring(build.getCause(), 0, MAX_SIZE));
					map.put(BuildMapping.RESULT, build.getResult());
					map.put(BuildMapping.DURATION, build.getDuration());
					map.put(BuildMapping.TIME_STAMP, build.getTimestamp());
					map.put(BuildMapping.BUILT_ON, build.getBuiltOnIfSet());
					map.put(BuildMapping.DELETED, build.isDeleted());
					mapping = ao.create(BuildMapping.class, map);
					mapping = ao.find(BuildMapping.class, Query.select().where("ID = ?", mapping.getID()))[0];
				} else {
					mapping = ao.get(BuildMapping.class, build.getId());
					mapping.setJobId(build.getJobId());
					mapping.setBuildNumber(build.getNumber());
					if (StringUtils.length(build.getDisplayName()) <= MAX_SIZE) {
						mapping.setDisplayName(build.getDisplayName());
					} else {
						LOGGER.warn("Display name of build {} of job {} is to big to store...", build.getNumber(), build.getJobId());
					}
					mapping.setCause(StringUtils.substring(build.getCause(), 0, MAX_SIZE));
					mapping.setResult(build.getResult());
					mapping.setDuration(build.getDuration());
					mapping.setTimeStamp(build.getTimestamp());
					mapping.setBuiltOn(build.getBuiltOnIfSet());
					mapping.setDeleted(build.isDeleted());
					mapping.save();
				}
				return mapping;
			}
		});
		return buildMappingToBuildFunction.apply(mapping);
	}

	@Override
	public int remove(int buildId) {
		ao.delete(ao.get(BuildMapping.class, buildId));
		return buildId;
	}

	@Override
	public int[] removeAllByJob(final int jobId) {
		return ao.executeInTransaction(new TransactionCallback<int[]>() {
			@Override
			public int[] doInTransaction() {
				BuildMapping[] mappings = ao.find(BuildMapping.class, Query.select().where(JOB_ID + "= ?", jobId));
				if (mappings == null) {
					return new int[0];
				} else {
					int[] buildIds = new int[mappings.length];
					for (int i = 0; i < mappings.length; i++) {
						buildIds[i] = mappings[i].getID();
					}
					ao.delete(mappings);
					return buildIds;
				}
			}
		});
	}

	@Override
	public void delete(Build build) {
		build.setDeleted(true);
		save(build);
	}

	@Override
	public void deleteAllInJob(int jobId) {
		deleteAllInJob(jobId, -1);
	}

	@Override
	public void deleteAllInJob(final int jobId, final int buildNumber) {
		ao.executeInTransaction(new TransactionCallback<Object>() {
			@Override
			public Object doInTransaction() {
				Query query = Query.select();
				if (buildNumber > 0) {
					query.where(JOB_ID + " = ? AND " + BUILD_NUMBER + " < ?", jobId, buildNumber);
				} else {
					query.where(JOB_ID + " = ?", jobId);
				}
				BuildMapping[] mappings = ao.find(BuildMapping.class, query);
				for (BuildMapping mapping : mappings) {
					mapping.setDeleted(true);
					mapping.save();
				}
				return null;
			}
		});
	}

}
