/*
 * Jenkins Plugin for JIRA
 * Copyright (C) 2012 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.jira.plugins.jenkins.dao.impl;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.jira.plugins.dvcs.dao.impl.MapRemovingNullCharacterFromStringValues;
import com.atlassian.sal.api.transaction.TransactionCallback;
import com.marvelution.jira.plugins.jenkins.ao.TestResultsMapping;
import com.marvelution.jira.plugins.jenkins.dao.TestResultDao;
import com.marvelution.jira.plugins.jenkins.model.TestResults;
import net.java.ao.Query;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

/**
 * Default {@link com.marvelution.jira.plugins.jenkins.dao.TestResultDao} implementation
 *
 * @author Mark Rekveld
 * @since 1.4.3
 */
public class DefaultTestResultsDao implements TestResultDao {

	private static final Logger LOGGER = LoggerFactory.getLogger(DefaultTestResultsDao.class);
	private final ActiveObjects ao;

	public DefaultTestResultsDao(ActiveObjects ao) {
		this.ao = ao;
	}

	@Override
	public TestResults getForBuild(final int buildId) {
		final TestResultsMapping mapping = ao.executeInTransaction(new TransactionCallback<TestResultsMapping>() {
			@Override
			public TestResultsMapping doInTransaction() {
				TestResultsMapping[] mappings = ao.find(TestResultsMapping.class, Query.select().where(TestResultsMapping.BUILD_ID + " = ?",
						buildId));
				if (mappings != null && mappings.length == 1) {
					return mappings[0];
				} else {
					return null;
				}
			}
		});
		return toTestResults(mapping);
	}

	@Override
	public TestResults save(final int buildId, final TestResults results) {
		if (results == null) {
			return null;
		}
		LOGGER.debug("Saving {}", results.toString());
		TestResultsMapping mapping = ao.executeInTransaction(new TransactionCallback<TestResultsMapping>() {
			@Override
			public TestResultsMapping doInTransaction() {
				TestResultsMapping mapping;
				if (results.getId() == 0) {
					Map<String, Object> map = new MapRemovingNullCharacterFromStringValues();
					map.put(TestResultsMapping.BUILD_ID, buildId);
					map.put(TestResultsMapping.TOTAL, results.getTotal());
					map.put(TestResultsMapping.SKIPPED, results.getSkipped());
					map.put(TestResultsMapping.FAILED, results.getFailed());
					mapping = ao.create(TestResultsMapping.class, map);
					mapping = ao.find(TestResultsMapping.class, Query.select().where("ID = ?", mapping.getID()))[0];
				} else {
					mapping = ao.get(TestResultsMapping.class, results.getId());
					mapping.setBuildId(buildId);
					mapping.setTotal(results.getTotal());
					mapping.setSkipped(results.getSkipped());
					mapping.setFailed(results.getFailed());
					mapping.save();
				}
				return mapping;
			}
		});
		return toTestResults(mapping);
	}

	/**
	 * Transform the given {@link com.marvelution.jira.plugins.jenkins.ao.TestResultsMapping} to a {@link
	 * com.marvelution.jira.plugins.jenkins.model.TestResults}
	 *
	 * @param mapping the {@link com.marvelution.jira.plugins.jenkins.ao.TestResultsMapping} to transform
	 * @return the {@link com.marvelution.jira.plugins.jenkins.model.TestResults}
	 */
	private TestResults toTestResults(TestResultsMapping mapping) {
		if (mapping == null) {
			return null;
		}
		return new TestResults(mapping.getID(), mapping.getBuildId(), mapping.getFailed(), mapping.getSkipped(), mapping.getTotal());
	}

	@Override
	public void removeTestResults(final int[] buildIds) {
		if (buildIds != null && buildIds.length > 0) {
			ao.executeInTransaction(new TransactionCallback<Object>() {
				@Override
				public Object doInTransaction() {
					TestResultsMapping[] mappings = ao.find(TestResultsMapping.class, Query.select().where(TestResultsMapping.BUILD_ID +
							" IN (" + StringUtils.join(ArrayUtils.toObject(buildIds), ",") + ")"));
					ao.delete(mappings);
					return null;
				}
			});
		}
	}

}
