/*
 * Jenkins Plugin for JIRA
 * Copyright (C) 2012 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.jira.plugins.jenkins.upgrade;

import com.atlassian.sal.api.message.Message;
import com.atlassian.sal.api.upgrade.PluginUpgradeTask;
import com.google.common.collect.Sets;
import com.marvelution.jira.plugins.jenkins.dao.JobDao;
import com.marvelution.jira.plugins.jenkins.model.Job;
import com.marvelution.jira.plugins.jenkins.services.SiteService;
import com.marvelution.jira.plugins.jenkins.utils.JenkinsPluginUtil;

import java.util.Collection;
import java.util.Collections;
import java.util.Set;

/**
 * {@link PluginUpgradeTask} implementation to cleanup the Jenkins and Hudson data
 *
 * @author Mark Rekveld
 * @since 1.4.7
 */
public class JobCleanupUpgradeTask implements PluginUpgradeTask {

	private final JenkinsPluginUtil pluginUtil;
	private final JobDao jobDao;
	private final SiteService siteService;

	public JobCleanupUpgradeTask(JenkinsPluginUtil pluginUtil, JobDao jobDao, SiteService siteService) {
		this.pluginUtil = pluginUtil;
		this.jobDao = jobDao;
		this.siteService = siteService;
	}

	@Override
	public int getBuildNumber() {
		return 1;
	}

	@Override
	public String getShortDescription() {
		return "Cleanup plugin job data";
	}

	@Override
	public Collection<Message> doUpgrade() throws Exception {
		Set<Integer> siteIds = Sets.newHashSet();
		for (Job job : jobDao.getAll(true)) {
			if (!siteIds.contains(job.getSiteId()) && siteService.get(job.getSiteId()) == null) {
				siteIds.add(job.getSiteId());
			}
		}
		for (Integer siteId : siteIds) {
			jobDao.removeAllBySiteId(siteId);
		}
		return Collections.emptyList();
	}

	@Override
	public String getPluginKey() {
		return pluginUtil.getPluginKey();
	}

}
