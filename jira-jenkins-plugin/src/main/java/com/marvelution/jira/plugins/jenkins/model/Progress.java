/*
 * Jenkins Plugin for JIRA
 * Copyright (C) 2012 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.jira.plugins.jenkins.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Mark Rekveld
 * @since 1.0.0
 */
@XmlRootElement(name = "progress")
@XmlAccessorType(XmlAccessType.FIELD)
public class Progress {

	private volatile boolean shouldStop = false;
	private long startTime = 0L;
	@XmlElement
	private boolean finished = false;
	@XmlElement
	private String error;
	@XmlElement
	private int buildCount = 0;
	@XmlElement
	private int issueCount = 0;
	@XmlElement
	private int syncErrorCount = 0;

	/**
	 * Called when the synchronization is started to record the start time
	 */
	public void start() {
		startTime = System.currentTimeMillis();
	}

	/**
	 * Called when the synchronization is finished
	 */
	public void finish() {
		finished = true;
	}

	/**
	 * Called when the synchronization operation is queued
	 */
	public void queued() {
	}

	/**
	 * Update the progress by updating the build count, issue count and sync error count
	 *
	 * @param buildCount     the number of builds processed
	 * @param issueCount     the  number of issues found
	 * @param syncErrorCount the number of error that occurred
	 */
	public void updateProgress(int buildCount, int issueCount, int syncErrorCount) {
		this.buildCount = buildCount;
		this.issueCount = issueCount;
		this.syncErrorCount = syncErrorCount;
	}

	/**
	 * Getter for startTime
	 *
	 * @return the startTime
	 */
	public long getStartTime() {
		return startTime;
	}

	/**
	 * Getter for finished
	 *
	 * @return the finished
	 */
	public boolean isFinished() {
		return finished;
	}

	/**
	 * Getter for shouldStop
	 *
	 * @return the shouldStop
	 */
	public boolean isShouldStop() {
		return shouldStop;
	}

	/**
	 * Setter for shouldStop
	 *
	 * @param shouldStop the shouldStop to set
	 */
	public void setShouldStop(boolean shouldStop) {
		this.shouldStop = shouldStop;
	}

	/**
	 * Getter for error
	 *
	 * @return the error
	 */
	public String getError() {
		return error;
	}

	/**
	 * Setter for error
	 *
	 * @param error the error to set
	 */
	public void setError(String error) {
		this.error = error;
	}

	/**
	 * Getter for buildCount
	 *
	 * @return the buildCount
	 */
	public int getBuildCount() {
		return buildCount;
	}

	/**
	 * Getter for issueCount
	 *
	 * @return the issueCount
	 */
	public int getIssueCount() {
		return issueCount;
	}

	/**
	 * Getter for syncErrorCount
	 *
	 * @return the syncErrorCount
	 */
	public int getSyncErrorCount() {
		return syncErrorCount;
	}

}
