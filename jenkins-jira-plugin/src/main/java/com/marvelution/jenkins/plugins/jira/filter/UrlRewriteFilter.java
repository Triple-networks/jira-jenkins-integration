/*
 * JIRA Plugin for Jenkins
 * Copyright (C) 2012 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.jenkins.plugins.jira.filter;

import org.tuckey.web.filters.urlrewrite.Conf;
import org.tuckey.web.filters.urlrewrite.UrlRewriter;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * {@link javax.servlet.Filter} implementation for rewriting URL
 *
 * @author Mark Rekveld
 * @since 1.4.4
 */
public class UrlRewriteFilter implements Filter {

	public static final String URLREWRITE_XML = "urlrewrite.xml";
	private UrlRewriter rewriter;

	public UrlRewriter getRewriter() {
		return rewriter;
	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		rewriter = new UrlRewriter(new Conf(filterConfig.getServletContext(), getClass().getResourceAsStream(URLREWRITE_XML),
				URLREWRITE_XML, "jenkins"));
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		if (!rewriter.processRequest((HttpServletRequest) request, (HttpServletResponse) response, chain)) {
			chain.doFilter(request, response);
		}
	}

	@Override
	public void destroy() {
		rewriter.destroy();
	}

}
