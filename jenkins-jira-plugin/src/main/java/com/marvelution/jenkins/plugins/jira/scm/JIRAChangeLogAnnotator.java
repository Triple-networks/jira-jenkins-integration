/*
 * JIRA Plugin for Jenkins
 * Copyright (C) 2012 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.jenkins.plugins.jira.scm;

import com.marvelution.jenkins.plugins.jira.model.ApplicationLink;
import com.marvelution.jenkins.plugins.jira.model.EntityLink;
import com.marvelution.jenkins.plugins.jira.store.ApplicationLinkStore;
import com.marvelution.jenkins.plugins.jira.store.EntityLinkStore;
import hudson.Extension;
import hudson.MarkupText;
import hudson.model.AbstractBuild;
import hudson.scm.ChangeLogAnnotator;
import hudson.scm.ChangeLogSet;
import org.apache.commons.lang.StringUtils;

import java.io.IOException;
import java.util.Map;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * {@link ChangeLogAnnotator} implementation that annotated JIRA issues keys that of related Entity Links
 *
 * @author Mark Rekveld
 * @since 1.2.0
 */
@Extension
public class JIRAChangeLogAnnotator extends ChangeLogAnnotator {

	private static final Logger LOGGER = Logger.getLogger(JIRAChangeLogAnnotator.class.getName());
	private static final String ISSUE_PATTERN_PREFIX = "(";
	private static final String ISSUE_PATTERN_SUFFIX = "+-[1-9][0-9]*)([^.]|\\.[^0-9]|\\.$|$)";

	@Override
	public void annotate(AbstractBuild<?, ?> build, ChangeLogSet.Entry entry, MarkupText markupText) {
		ApplicationLinkStore applicationStore;
		try {
			applicationStore = getApplicationLinkStore();
		} catch (IOException e) {
			LOGGER.warning("Failed to load the Application Link store");
			return;
		}
		try {
			EntityLinkStore linkStore = getEntityLinkStore(build);
			if (linkStore != null && linkStore.hasLinks()) {
				for (Map.Entry<String, EntityLink> link : linkStore.getAsMap().entrySet()) {
					ApplicationLink applink = applicationStore.get(link.getValue());
					if (applink != null) {
						Pattern pattern = Pattern.compile(ISSUE_PATTERN_PREFIX + link.getKey() + ISSUE_PATTERN_SUFFIX);
						Matcher matcher = pattern.matcher(markupText.getText());
						while (matcher.find()) {
							if (matcher.groupCount() >= 1) {
								String url = StringUtils.stripEnd(applink.getDisplayUrl(), "/")
										+ "/browse/" + matcher.group(1);
								markupText.addMarkup(matcher.start(1), matcher.end(1), "<a href='" + url + "'>", "</a>");
							}
						}
					}
				}
			}
		} catch (IOException e) {
			LOGGER.warning("Failed to load the Entity Link of project " + build.getProject().getName());
		}
	}

	/**
	 * Get the {@link EntityLinkStore} for the given {@link AbstractBuild}
	 *
	 * @param build the {@link AbstractBuild} to get the {@link EntityLinkStore} for
	 * @return the {@link EntityLinkStore}
	 * @throws IOException in case of {@link com.marvelution.jenkins.plugins.jira.store.EntityLinkStore#load()} errors
	 */
	EntityLinkStore getEntityLinkStore(AbstractBuild<?, ?> build) throws IOException {
		return EntityLinkStore.getStore(build.getProject());
	}

	/**
	 * Get the {@link ApplicationLinkStore}
	 *
	 * @return the {@link ApplicationLinkStore}
	 * @throws IOException in case of {@link com.marvelution.jenkins.plugins.jira.store.ApplicationLinkStore#load()}
	 *                     errors
	 */
	ApplicationLinkStore getApplicationLinkStore() throws IOException {
		return ApplicationLinkStore.getStore();
	}

}
