/*
 * JIRA Plugin for Jenkins
 * Copyright (C) 2012 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.jenkins.plugins.jira.applinks;

import com.marvelution.jenkins.plugins.jira.model.ApplicationLink;
import com.marvelution.jenkins.plugins.jira.store.ApplicationLinkStore;
import com.marvelution.jenkins.plugins.jira.utils.XStreamUtils;
import hudson.Extension;
import hudson.model.Hudson;
import org.apache.commons.lang.StringUtils;
import org.kohsuke.stapler.StaplerRequest;
import org.kohsuke.stapler.StaplerResponse;

import javax.servlet.http.HttpServletResponse;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Handler for the {@code /rest/applinks/1.0/applicationlink/*} request from JIRA
 *
 * @author Mark Rekveld
 * @since 1.1.0
 */
@Extension
public class ApplicationlinkAction extends ApplicationLinksAction {

	private static final Logger LOGGER = Logger.getLogger(ApplicationlinkAction.class.getName());
	public static final String ACTION = "applicationlink";

	@Override
	public String getUrlName() {
		return ACTION;
	}

	public void doDynamic(StaplerRequest request, StaplerResponse response) throws Exception {
		ApplicationLinkStore store = ApplicationLinkStore.getStore();
		String pathInfo = request.getPathInfo();
		String rest = pathInfo.substring(pathInfo.indexOf(ACTION) + ACTION.length());
		String appId = StringUtils.stripEnd(StringUtils.stripStart(rest, "/"), "/");
		if (hasPermission(Hudson.getInstance(), Hudson.ADMINISTER)) {
			if ("PUT".equals(request.getMethod())) {
				try {
					ApplicationLink applicationLink = XStreamUtils.getEntityFromRequest(request.getInputStream(),
							ApplicationLink.class);
					LOGGER.info("Adding Application Link [" + appId + "] to " + applicationLink.getName() + " at " +
							applicationLink.getRpcUrl());
					store.add(appId, applicationLink);
					response.setStatus(HttpServletResponse.SC_CREATED);
				} catch (Exception e) {
					LOGGER.log(Level.SEVERE, "Failed to add ApplicationLink for " + appId, e);
					response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Failed to add ApplicationLink for " + appId);
				}
			} else if ("DELETE".equals(request.getMethod())) {
				LOGGER.info("Deleting Application Link with ID " + appId);
				store.remove(appId);
				response.setStatus(HttpServletResponse.SC_OK);
			} else {
				response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Unsupported operation!");
			}
		} else {
			response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
		}
	}

}
