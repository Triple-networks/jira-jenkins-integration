/*
 * JIRA Plugin for Jenkins
 * Copyright (C) 2012 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.jenkins.plugins.jira.oauth.utils;

import com.atlassian.oauth.Consumer;
import com.atlassian.oauth.serviceprovider.ServiceProviderToken;
import com.atlassian.oauth.util.RSAKeys;
import com.marvelution.jenkins.plugins.jira.model.ConsumerInfo;
import net.oauth.OAuth;
import net.oauth.OAuthConsumer;
import net.oauth.signature.RSA_SHA1;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

/**
 * Utility class for Consumer related operations
 *
 * @author Mark Rekveld
 * @since 1.4.0
 */
public class ConsumerUtils {

	static final String NAME = "name";
	static final String DESCRIPTION = "description";

	/**
	 * Convert a given {@code consumerInfo} to a {@link com.atlassian.oauth.Consumer}
	 *
	 * @param consumerInfo the {@link com.marvelution.jenkins.plugins.jira.model.ConsumerInfo} to convert
	 * @return the {@link com.atlassian.oauth.Consumer}
	 * @throws InvalidKeySpecException
	 * @throws NoSuchAlgorithmException
	 */
	public static Consumer toConsumer(ConsumerInfo consumerInfo) throws InvalidKeySpecException, NoSuchAlgorithmException {
		return Consumer.key(consumerInfo.getKey()).name(consumerInfo.getName()).description(consumerInfo.getDescription())
				.publicKey(RSAKeys.fromPemEncodingToPublicKey(consumerInfo.getPublicKey())).build();
	}

	/**
	 * Convert a given {@code consumerInfo} to an {@link net.oauth.OAuthConsumer}
	 *
	 * @param consumerInfo the {@link com.marvelution.jenkins.plugins.jira.model.ConsumerInfo} to convert
	 * @param callback     the callback uri
	 * @return the {@link net.oauth.OAuthConsumer}
	 * @throws InvalidKeySpecException
	 * @throws NoSuchAlgorithmException
	 */
	public static OAuthConsumer toOAuthConsumer(ConsumerInfo consumerInfo, String callback) throws InvalidKeySpecException,
			NoSuchAlgorithmException {
		final OAuthConsumer consumer = new OAuthConsumer(callback, consumerInfo.getKey(), null, null);
		consumer.setProperty(NAME, consumerInfo.getName());
		consumer.setProperty(DESCRIPTION, consumerInfo.getDescription());
		consumer.setProperty(OAuth.OAUTH_SIGNATURE_METHOD, OAuth.RSA_SHA1);
		consumer.setProperty(RSA_SHA1.PUBLIC_KEY, RSAKeys.fromPemEncodingToPublicKey(consumerInfo.getPublicKey()));
		return consumer;
	}

	/**
	 * Convert a given {@code token} to an {@link net.oauth.OAuthConsumer}
	 *
	 * @param token the {@link com.atlassian.oauth.serviceprovider.ServiceProviderToken} to convert
	 * @return the {@link net.oauth.OAuthConsumer}
	 */
	public static OAuthConsumer toOAuthConsumer(ServiceProviderToken token) {
		String callback = token.getCallback() != null ? token.getCallback().toString() : null;
		final OAuthConsumer consumer = new OAuthConsumer(callback, token.getConsumer().getKey(), null, null);
		consumer.setProperty(NAME, token.getConsumer().getName());
		consumer.setProperty(DESCRIPTION, token.getConsumer().getDescription());
		consumer.setProperty(OAuth.OAUTH_SIGNATURE_METHOD, OAuth.RSA_SHA1);
		consumer.setProperty(RSA_SHA1.PUBLIC_KEY, token.getConsumer().getPublicKey());
		return consumer;
	}

}
