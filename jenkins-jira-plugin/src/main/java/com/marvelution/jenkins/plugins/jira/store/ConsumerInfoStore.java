/*
 * JIRA Plugin for Jenkins
 * Copyright (C) 2012 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.jenkins.plugins.jira.store;

import com.google.common.collect.Maps;
import com.marvelution.jenkins.plugins.jira.model.ConsumerInfo;
import hudson.model.Hudson;

import java.io.IOException;
import java.util.Collection;
import java.util.Map;

/**
 * Store for {@link com.marvelution.jenkins.plugins.jira.model.ConsumerInfo} objects
 *
 * @author Mark Rekveld
 * @since 1.1.0
 */
public class ConsumerInfoStore extends BaseStore {

	private static transient ConsumerInfoStore STORE;
	private final Map<String, ConsumerInfo> consumers = Maps.newHashMap();

	/**
	 * Private constructor
	 */
	private ConsumerInfoStore() {
		super(Hudson.getInstance().getRootDir(), "applinks-consumer-info.xml");
	}

	/**
	 * Getter for the Store instance
	 *
	 * @return the {@link com.marvelution.jenkins.plugins.jira.store.ConsumerInfoStore}
	 * @throws java.io.IOException in case of store load errors
	 */
	public static ConsumerInfoStore getStore() throws IOException {
		if (STORE == null) {
			STORE = new ConsumerInfoStore();
			STORE.load();
		}
		return STORE;
	}

	/**
	 * Getter for all the configured Consumer Info objects
	 *
	 * @return all the configured Consumer Info objects
	 */
	public Collection<ConsumerInfo> getAll() {
		return consumers.values();
	}

	/**
	 * Get an {@link com.marvelution.jenkins.plugins.jira.model.ConsumerInfo} by its key
	 *
	 * @param key the Consumer Info key
	 * @return the {@link com.marvelution.jenkins.plugins.jira.model.ConsumerInfo}, may be {@code null}
	 */
	public ConsumerInfo get(final String key) {
		return consumers.get(key);
	}

	/**
	 * Add a new {@link com.marvelution.jenkins.plugins.jira.model.ConsumerInfo}
	 *
	 * @param consumerInfo the {@link com.marvelution.jenkins.plugins.jira.model.ConsumerInfo}
	 * @throws java.io.IOException in case of store save errors
	 */
	public synchronized void add(ConsumerInfo consumerInfo) throws IOException {
		consumers.put(consumerInfo.getKey(), consumerInfo);
		save();
	}

	/**
	 * Remove the {@link com.marvelution.jenkins.plugins.jira.model.ConsumerInfo} by its key
	 *
	 * @param key the consumer key to remove
	 * @throws java.io.IOException in case of store save errors
	 */
	public synchronized void remove(String key) throws IOException {
		consumers.remove(key);
		save();
	}

}
