/*
 * JIRA Plugin for Jenkins
 * Copyright (C) 2012 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.jenkins.plugins.jira.oauth.xstream;

import com.atlassian.oauth.Consumer;
import com.marvelution.jenkins.plugins.jira.oauth.utils.ConsumerUtils;
import com.marvelution.jenkins.plugins.jira.store.ConsumerInfoStore;
import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

import java.util.logging.Logger;

/**
 * {@link com.thoughtworks.xstream.converters.Converter} to convert {@link com.atlassian.oauth.Consumer} objects
 *
 * @author Mark Rekveld
 * @since 1.4.0
 */
public class ConsumerConverter implements Converter {

	private static final Logger LOGGER = Logger.getLogger(ConsumerConverter.class.getName());

	@Override
	public void marshal(Object value, HierarchicalStreamWriter writer, MarshallingContext context) {
		writer.setValue(((Consumer) value).getKey());
	}

	@Override
	public Object unmarshal(HierarchicalStreamReader reader, UnmarshallingContext context) {
		String key = reader.getValue();
		try {
			return ConsumerUtils.toConsumer(ConsumerInfoStore.getStore().get(key));
		} catch (Exception e) {
			LOGGER.warning("Failed to load consumer for key: " + key);
			return null;
		}
	}

	@Override
	public boolean canConvert(Class type) {
		return Consumer.class.equals(type);
	}

}
